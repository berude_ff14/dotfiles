if exists('b:did_indent')
  finish
endif

setlocal shiftwidth=2 softtabstop=2 tabstop=2 expandtab

" let b:did_indent = 1
