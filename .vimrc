set encoding=utf-8
scriptencoding utf-8

set fileencoding=utf-8
set fileencodings=utf-8,euc-jp
set fileformats=unix,dos,mac
set ambiwidth=double

filetype off
if has('vim_starting')
  set rtp^=~/.vim/miv/miv/
endif

syntax on
filetype plugin indent on
set smartindent
set smarttab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set ignorecase
set smartcase
set hlsearch
set incsearch
set title
set number
set showcmd
set showtabline=2
set laststatus=2
set cursorline
set history=500
set wildmenu
set list
set listchars=tab:>-
set showmatch
set matchtime=1
set foldmethod=marker
set foldcolumn=0
set lazyredraw
set backspace=indent,eol,start
set display=lastline
set pumheight=10

augroup swapchoice-readonly
  autocmd!
  autocmd SwapExists * let v:swapchoice = 'o'
augroup END

let mapleader = "\<Space>"

nnoremap Y y$
