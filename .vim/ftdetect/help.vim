autocmd BufNewFile,BufRead *.txt call s:set_help_settings()
function! s:set_help_settings()
  if expand('%:p') =~ 'doc/.*\.txt$'
    setlocal filetype=help
  endif
endfunction
